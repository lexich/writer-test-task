chrome.runtime.onInstalled.addListener(() => {
  chrome.storage.sync.set({
    json: [
      {
        from: 0,
        to: 5,
      },
      {
        from: 12,
        to: 15,
      },
      {
        from: 30,
        to: 33,
      },
    ],
  });
});
