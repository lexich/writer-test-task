export interface IConfig {
  from: number;
  to: number;
}

export const loadJSON = () =>
  new Promise<IConfig[]>((resolve) => {
    chrome.storage.sync.get('json', (d) => {
      resolve(d.json ?? []);
    });
  });
