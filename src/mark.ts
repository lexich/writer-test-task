import { isTextNode } from './utils';
import { HIGHLIGHTED } from './markClass';

export const markTextNode = (textNode: Text, from: number, to: number) => {
  if (!textNode || !textNode.length || !textNode.textContent) {
    return { from, to };
  }
  const textContent = textNode.textContent;
  const wrappedText = textContent.slice(from, to);
  if (!wrappedText) {
    return {
      from: from - textContent.length,
      to: to - textContent.length,
    };
  }

  const frag = document.createDocumentFragment();
  if (from) {
    const node = document.createTextNode(textContent.slice(0, from));
    frag.appendChild(node);
  }
  const wrap = document.createElement('mark');
  wrap.className = HIGHLIGHTED;
  wrap.innerHTML = wrappedText;
  frag.appendChild(wrap);
  if (to < textContent.length) {
    const node = document.createTextNode(textContent.slice(to));
    frag.appendChild(node);
  }
  if (textNode.parentNode) {
    textNode.parentNode.replaceChild(frag, textNode);
  }

  if (to < textContent.length) {
    return null; // text highlighted
  }
  const wrapped = textContent.length - from;
  return {
    from: from - wrapped,
    to: to - wrapped,
  };
};

export const markNode = (el: Node, from: number, to: number) => {
  for (const child of el.childNodes) {
    if (isTextNode(child)) {
      const ret = markTextNode(child, from, to);
      if (ret) {
        from = ret.from;
        to = ret.to;
        if (from < 0) {
          return false;
        }
      } else {
        return false;
      }
    } else {
      const result = markNode(child, from, to);
      if (!result) {
        return false;
      }
    }
  }
  return true;
};

export const unMarkNodes = (el: HTMLElement) => {
  const marks = el.querySelectorAll(`mark.${HIGHLIGHTED}`);
  Array.from(marks).forEach((mark) => {
    if (!mark.parentElement) {
      return;
    }
    const nodes = Array.from(mark.parentElement.childNodes);
    const index = nodes.indexOf(mark);
    const newNode = document.createTextNode('');
    let beforeNode = index ? nodes[index - 1] : null;
    if (beforeNode && beforeNode.nodeType === 3) {
      newNode.insertData(
        newNode.textContent?.length ?? 0,
        beforeNode.textContent ?? ''
      );
    } else {
      beforeNode = null;
    }
    newNode.insertData(
      newNode.textContent?.length ?? 0,
      mark.textContent ?? ''
    );
    let afterNode = index + 1 < nodes.length ? nodes[index + 1] : null;
    if (afterNode && afterNode.nodeType === 3) {
      newNode.insertData(
        newNode.textContent?.length ?? 0,
        afterNode.textContent ?? ''
      );
    } else {
      afterNode = null;
    }

    mark.parentNode && mark.parentNode.replaceChild(newNode, mark);
    beforeNode && beforeNode.remove();
    afterNode && afterNode.remove();
  });
};
