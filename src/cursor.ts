import { isTextNode } from './utils';

function createRange(
  node: Node,
  chars: number,
  range?: Range
): [Range, number] {
  if (!range) {
    range = document.createRange();
    range.selectNode(node);
    range.setStart(node, 0);
  }

  if (chars === 0) {
    range.setEnd(node, chars);
  } else if (node && chars > 0) {
    if (isTextNode(node)) {
      if (node.textContent && node.textContent.length < chars) {
        chars -= node.textContent.length;
      } else {
        range.setEnd(node, chars);
        chars = 0;
      }
    } else {
      for (var lp = 0; lp < node.childNodes.length; lp++) {
        const [newRange, newChars] = createRange(
          node.childNodes[lp],
          chars,
          range
        );
        range = newRange;
        chars = newChars;

        if (chars === 0) {
          break;
        }
      }
    }
  }

  return [range, chars];
}

function setCurrentCursorPosition(node: HTMLElement, chars: number) {
  if (chars >= 0) {
    const selection = window.getSelection();
    const [range] = createRange(node, chars);

    if (range) {
      range.collapse(false);
      if (selection) {
        selection.removeAllRanges();
        selection.addRange(range);
        node.focus();
      }
    }
  }
}

export function keepCaretePosition(node: HTMLElement, exec: () => void) {
  const selection = window.getSelection();
  if (!selection) {
    exec();
    return;
  }
  let index: number | null = null;

  if (selection.rangeCount > 0) {
    const selRange = selection.getRangeAt(0);
    const range = document.createRange();
    range.selectNodeContents(node);
    range.setEnd(selRange.startContainer, selRange.startOffset);
    const textContent = range.cloneContents().textContent;
    index = textContent?.length ?? null;
  }

  exec();

  if (index) {
    setCurrentCursorPosition(node, index);
  }
}
