import { ready } from './ready';
import { IConfig, loadJSON } from './json';
import { markNode, unMarkNodes } from './mark';
import { keepCaretePosition } from './cursor';

ready(() => {
  const isContentEditable = (element: EventTarget | null) => {
    if (!element) {
      return;
    }
    const el = element as HTMLElement;
    if (el.hasAttribute && el.hasAttribute('contenteditable')) {
      const val = el.getAttribute('contenteditable');
      return val === 'false' || val === '0' ? false : true;
    }
    return false;
  };

  const highlightedElement = new Set<HTMLElement>();

  const unHighlight = (el: HTMLElement) => {
    if (highlightedElement.has(el)) {
      unMarkNodes(el);
      highlightedElement.delete(el);
    }
  };

  let JSON: IConfig[] = [];

  loadJSON().then((json) => (JSON = json));

  const highlight = (el: HTMLElement) => {
    unHighlight(el);
    for (const conf of JSON) {
      markNode(el, conf.from, conf.to);
    }

    highlightedElement.add(el);
  };
  let delay: any;
  document.body.addEventListener('focusin', (ev) => {
    if (!isContentEditable(ev.target)) {
      return;
    }
    highlight(ev.target as HTMLElement);
  });
  document.body.addEventListener('focusout', (ev) => {
    if (!isContentEditable(ev.target)) {
      return;
    }
    unHighlight(ev.target as HTMLElement);
    if (delay) {
      clearTimeout(delay);
      delay = null;
    }
  });

  document.body.addEventListener(
    'keyup',
    (ev) => {
      if (!isContentEditable(ev.target)) {
        return;
      }

      if (delay) {
        clearTimeout(delay);
      }
      const el = ev.target as HTMLElement;
      keepCaretePosition(el, () => {
        unHighlight(el);
      });
      setTimeout(() => {
        delay = setTimeout(() => {
          keepCaretePosition(el, () => {
            highlight(el);
          });
          delay = null;
        }, 500);
      }, 0);
    },
    false
  );
});
