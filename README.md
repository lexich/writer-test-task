### [Task](https://docs.google.com/document/d/1YrA-eWdP6k-T8XsOVLh-lVB8vWkmAEmyb1a-QIWyjcA/edit)

### Description
To run application you need:

- Install dependencies `yarn`
- Build application `yarn build`
- Load unpacked from `dist` folder

### Development

To build project.

```
yarn build
```

To start project in development mode.

```
yarn dev
```

### Thoughts
```
The highlighting functionality should run as expected in the above 2 parts but you should be able to show the highlights without actually modifying the DOM.

It will be great to know your solutions/thoughts of handling this if for any reason you are unable to code this part.
```

The only idea is to draw highlighting blocks above text with `position: absolute` positioning. But I guess that there are a lot of edge cases where this solution will have lousy behavior (e.g., text block has `position: fixed` or resize browser) window.
